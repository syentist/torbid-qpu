# torbid-qpu

TorbidQPU is a TensorFlow 2 distribution strategy optimized for quantum computing and decentralized networks.

TorbidQPU is an automated modular data science workflow powered by the decentralized Ocean Protocol framework that facilitates the creation and training of quantum neural networks.

TorbidQPU will be deployed as an Android app via TensorFlow Lite and a web app via TensorFlow.js.